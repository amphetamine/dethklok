# dethklok

A Clojure library for figuring how many seconds you've got left.

Inspired by the original(?) [Death Clock](http://www.deathclock.com/).

[![Clojars Project](https://img.shields.io/clojars/v/dethklok.svg)](https://clojars.org/dethklok)

## Usage

The main function this library provides is `lifespan`, which takes two
arguments: a `birthdate` string in `"YYYY MM dd"` format, and a `sex` string,
one of `"Male"` or `"Female"` (as those are the two most commonly computed in
actuarial tables).

The output is a number indicating the expected number of seconds a person with
that birth date and sex is expected to live.

## License

Copyright © 2016 Ashley Lake

Distributed under the Affero GNU Public License v3.
