(ns dethklok.core
  (:require [clojure.string :refer [split]]
            [clojure-csv.core :refer [parse-csv]]
            [clojure.edn :refer [read-string]]))

(def year-secs (* 365.25 24 60 60))

(defn third
  "gets the third element of a list."
  [list]
  (nth list 2))

(defn parse-int
  "from http://stackoverflow.com/a/12503724/2023432"
  [s]
  (Integer. (re-find  #"\d+" s)))

(defn lookup-life
  "looks up the life expectancy table value for a given age in years. current
  stats from USA (https://www.ssa.gov/OACT/STATS/table4c6.html), but should be
  expanded."
  [age sex]
  (let [actuary-table
        (parse-csv (slurp (.getPath (clojure.java.io/resource "data.csv"))))
        sex-ind (if (= sex "Female" ) 1 0)]
    (read-string (nth (nth actuary-table sex-ind) (+ age 1)))))

(defn parse-date
  "parses a birthdate into a usable time representation. expects YYYY/MM/DD."
  [date-string]
  (let [date-list (map parse-int (split date-string #" "))]
    {:year (first date-list) :month (second date-list) :day (third date-list)}))

(defn now
  "gets the current date in a form that we like"
  []
  (parse-date
   (.format (java.text.SimpleDateFormat. "yyyy MM dd") (new java.util.Date))))

(defn year-to-sec
  "converts a number in earth years to seconds"
  [year-num]
  (* year-secs year-num))

(defn lifespan
  "computes the expected lifespan in seconds of a person based on birthdate and
  sex."
  [birthdate sex]
  (let [year-age (- (:year (now)) (:year (parse-date birthdate)))]
    (year-to-sec (lookup-life year-age sex))))
