(ns dethklok.core-test
  (:require [clojure.test :refer :all]
            [dethklok.core :refer :all]))

(deftest test-parse-date
  (testing "parse-date produces a year month day date object"
    (is (= {:year 1990 :month 01 :day 01} (parse-date "1990 01 01")))))

(deftest test-lookup-life
  (testing "lookup-life produces the right number from CSV when called"
    (is (= 81.05 ;; don't forget to update this when you update the data
           (lookup-life 0 "Female")))))

(deftest test-year-to-sec
  (testing "conversion yields 365.25 days of seconds"
    (is (= year-secs (year-to-sec 1)))))

(deftest test-lifespan
  (testing "lifespan lookup produces the right number of seconds given a
  birthdate and sex"
    (is (< (- 2.55774348E9 (lifespan "2016 9 17" "Female")))) 0.0000001))
