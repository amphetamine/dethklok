(defproject dethklok "0.1.0-SNAPSHOT"
  :description "A library for calculating how many seconds you've got left."
  :url "https://gitlab.com/amphetamine/dethklok"
  :license {:name "Affero GNU Public License v3"
            :url "https://www.gnu.org/licenses/agpl-3.0.html"}
  :repl-options { :init-ns dethklok.core }
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [clojure-csv/clojure-csv "2.0.1"]])
